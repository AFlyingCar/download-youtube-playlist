# Download Youtube Playlist
A simple set of Bash and Powershell scripts that allows downloading a playlist of music from youtube.

## Installation
No installation of the script itself is necessary, however the script does depend on two other programs to run, [ffmpeg](https://www.ffmpeg.org/) and [youtube-dl](https://rg3.github.io/youtube-dl/). See the installation instructions for ffmpeg and youtube-dl for information on how to install them.

## Usage
```
dlYTPlaylist [OPTIONS]....
```

Do note that there are two different versions of the script. On Linux, run the `dlYTPlaylist.sh` script. On Windows, run either the `dlYTPlaylist.ps1` or the `dlYTPlaylist.bat` script. The batch script is simply a wrapper around the Powershell script if the Powershell ExecutionPolicy is set to disallow unknown scripts.

## Options
```
    -h       Display this message and exit.
    -m       Create a metadata file which stores the original URL.
    -u [URL] The URL to use.
    -d [DIR] The directory to save to.
    -q       Suppresses all standard output.
    -n       Does not generate an m3u playlist file.
    -v       Enables verbose output.
```

## Additional Information
As part of the download process, all downloaded files will be converted into `mp3` files, and have a name of `$VIDEO_NAME-v=$YOUTUBE_ID`. For example, this video: https://www.youtube.com/watch?v=nW8dGwa2zRw would download to a file titled `Typewriter - Brandenburger Symphoniker-v=nW8dGwa2zRw.mp3`.
